[![build status](https://gitlab.com/gaborigloi93/mrbayes/badges/master/build.svg)](https://gitlab.com/gaborigloi93/mrbayes/commits/master)

---

Plain HTML site hosted with GitLab Pages, storing the files referenced by the [mrbayes] tutorial at <https://gaborigloi93.gitlab.io/pelican>.

---

[mrbayes]: http://mrbayes.sourceforge.net/
